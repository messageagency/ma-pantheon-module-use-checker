#!/bin/bash

GREPOPT=""
for ARG in "$@" ; do
  GREPOPT="$GREPOPT -e $ARG"
done

ALL_SITES="$(terminus site:list --field=name)"
IFS=$' ' SORTED_SITES=($(sort <<<"${ALL_SITES[*]}")); unset IFS
for SITE in $SORTED_SITES ; do
  if [ $SITE != 'yalesite235' ] && [ $SITE != "yalesite30" ]
  then
    echo "# pml $SITE"
    terminus drush "$SITE.live" -- pml --status=enabled | grep $GREPOPT
  fi
done
