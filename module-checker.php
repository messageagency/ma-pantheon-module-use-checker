<?php

include_once 'vendor/autoload.php';

use Spatie\Async\Pool;

$pool = Pool::create();
$leave_out = ['yalesite235', 'yalesite30', ''];

$site_list = shell_exec('terminus site:list --field=name');
$site_list = explode(PHP_EOL, $site_list);
sort($site_list);

foreach($site_list as $site) {
  if (!in_array($site, $leave_out)) {
    $filename = 'site-reports/' . $site . '.txt';
    // print $filename . PHP_EOL;
    $pool->add(function() use ($site) {
      print $site . PHP_EOL;
      $command = 'terminus drush ' . $site . '.live -- pml --status=enabled';
      $output = shell_exec($command);
      print $output . PHP_EOL;
      return $output;
    })->then(function($output) use ($filename) {
      print $output . PHP_EOL;
      \file_put_contents($filename, $output);
    })->catch(function (\Throwable $exception) {
      // Do nothing.
    });
  }
}

$pool->wait();
